package com.team.ruffle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RuffleMachineApplication {

	public static void main(String[] args) {
		SpringApplication.run(RuffleMachineApplication.class, args);
	}

}
